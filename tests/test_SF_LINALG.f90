program test_SF_LINALG
  use SCIFOR
  use SF_LINALG
  integer, parameter :: nrows=3
  integer, parameter :: ncols=3

  real(8)    :: A(nrows,ncols), Ainv(nrows,ncols)
  real(8)    :: b(nrows)
  real(8)    :: x(nrows)
  integer :: irow, icol

  do irow=1,nrows 
    do icol=1,ncols 
      A(irow,icol)=random_normal()
    end do
    b(irow)=random_normal() 
  end do
  
  Ainv = A 
  call inv(Ainv)

  x = matmul(Ainv,b)

  write(*,*) "A"
  do irow=1,nrows 
    write(*,*) A(irow,:)
  enddo 

  write(*,*) "b"
  do irow=1,nrows 
    write(*,*) b(irow)
  enddo 

  write(*,*) "x"
  do irow=1,nrows 
    write(*,*) x(irow)
  enddo 

  b = matmul(A,x)
  write(*,*) "b"
  do irow=1,nrows 
    write(*,*) b(irow)
  enddo 
  
end program 